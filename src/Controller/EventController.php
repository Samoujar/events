<?php
namespace App\Controller;









use App\Entity\Event;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;






class EventController extends AbstractController
{
    /**
 * @Route("/test",name = "test")
 */

    public function newevent(Request $request){
        $em=$this->getDoctrine()->getManager();
        $event = new Event();

       $event->setName('said');
       $event->setAdresse('paris');
       $event->setDescription('Quelques mots ');
       $event->setDday(new \DateTime('tomorrow'));
       $event->setCreateur($this->getUser());


        //$date = DateTime::createFromFormat('d/m/Y');
        //$date = $date->format('Y-m-d');
        //$date->setTimeZone(new DateTimeZone('America/New_York'));

       $form = $this->createFormBuilder($event)
           ->add('name',TextType::class, array('attr'=>array('class'=>'form-control ml-3')))
           ->add("Dday", DateTimeType::class, array('attr'=>array('class'=>'col-2 col-form-label','for'=>"example-date-input")))
           ->add('adresse',TextType::class, array('attr'=>array('class'=>'form-control ml-3')))
           ->add('description',TextareaType::class, array('attr'=>array('class'=>'form-control ml-3')) )
           ->add('Valider',SubmitType::class, array('attr'=>array('class'=>'btn btn-primary mt-3 ml-3')))
           ->getForm();
       $form->handleRequest($request);

       if ($form->isSubmitted()&& $form->isValid()){
           $event = $form->getData();
           /*$id = $form->getData();
           $name=$form["name"]->getData();
           $dday = $form["dday"]->getData();
           $description = $form["description"]->getData();
           $adresse = $form["adresse"]->getData();*/

          //dump($event);
          //die();


           $em->persist($event);
           $em->flush();

         return $this->redirectToRoute("show");
       }

       return $this->render('event/newevent.html.twig',[
           'form'=>$form->createView(),
       ]);
    }
}