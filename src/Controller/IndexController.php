<?php


namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class IndexController extends AbstractController
{
    /**
     * @Route("/user/{id}" , name = "user")
     */

    public function base(User $user){

        return $this->render('user.html.twig',["user"=>$user]);
    }


}