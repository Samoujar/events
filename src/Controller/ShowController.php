<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use http\Env\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ShowController extends AbstractController
{

    private $eventRepository;
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @Route("/show",name = "show")
     */
    public function index(){
        // return new Response('<html><body>Hellow</body></html>');
        $events = $this->eventRepository->findAll();
//return ["events"=>$events];
//dd($events);

        //$events =['event 1','event 2'];

        return $this->render('showEvents/index.html.twig',["events"=>$events]);
    }

    /**
     * @Route("/event/delete/{id}" , name="delete_event")
     */
    public  function delete(Event $events ,$id){
        $events = $this->eventRepository->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($events);
        $entityManager->flush();

        return new Response("on efface".$events->getName());
    }

   /*public function delete(Response $response,$id){
       /*$events = $this->eventRepository->find($id);

       $entityManager = $this->getDoctrine()->getManager();
       $entityManager->remove($events);
       $entityManager->flush();

dd($events);
       //$response = new Response();
       //dd($response);
       //$response->send();
        //$id = $this->eventRepository->remove();

   }*/

}