const events = document.getElementById('table');

if(events){
    events.addEventListener('click',e=>{
        if (e.target.className === 'btn btn-danger delete-event'){
            if (confirm("êtes-vous sure de vouloire suprimer cette evenement ?")) {
                const id = e.target.getAttribute('data-id');
                fetch('/event/delete/{id}',
                    { method:'DELETE'}

                ).then(res =>window.location.reload())
            }
        }
    });
}